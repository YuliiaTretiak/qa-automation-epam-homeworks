package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ActivewarePage extends BasePage{

    @FindBy(xpath = "//button[@class='_1om7l06']/div[contains(text(),'Activity')]")
    private WebElement activityDropdownMenuButton;

    @FindBy(xpath = "//button[@class='_1om7l06']/div[contains(text(),'Colour')]")
    private WebElement colorDropdownMenuButton;

    @FindBy(xpath = "//ul//label[contains(@for,'attribute')]/div")
    private List<WebElement> activityButtons;

    @FindBy(xpath = "//ul//label[contains(@for,'colour')]/div")
    private List<WebElement> colorButtons;

    @FindBy(xpath = "//section/article")
    private List<WebElement> ChosenProducts;

    @FindBy(xpath = "//div[@data-auto-id='productList']")
    private WebElement productList;

    public ActivewarePage(WebDriver driver) {
        super(driver);
    }

    public boolean isActivityDropdownMenuButtonVisible() { return activityDropdownMenuButton.isDisplayed(); }

    public boolean isColorDropdownMenuButtonVisible() { return colorDropdownMenuButton.isDisplayed(); }

    public void clickActivityDropdownMenuButton() { activityDropdownMenuButton.click();}

    public void clickColorDropdownMenuButton() { colorDropdownMenuButton.click();}

    public boolean isColorButtonVisible(final String color)
    {
        for (WebElement item: colorButtons)
        {
            if (item.getText().contains(color)) { return true; }
        }
        return false;
    }

    public void clickColorButton(final String color)
    {
        for (WebElement item: colorButtons)
        {
            if (item.getText().contains(color)) { item.click(); }
        }
    }

    public boolean isActivityButtonVisible(final String activity)
    {
        for (WebElement item: activityButtons)
        {
            if (item.getText().contains(activity)) { return true; }
        }
        return false;
    }

    public void clickActivityButton(final String activity)
    {
        for (WebElement item: activityButtons)
        {
            if (item.getText().contains(activity)) { item.click(); }
        }
    }

    public List<WebElement> getChosenProductsList() {
        return ChosenProducts;
    }

    public int getAmountOfChosenProducts() {
        return getChosenProductsList().size();
    }

    public WebElement getProductList() {return productList;}
}
