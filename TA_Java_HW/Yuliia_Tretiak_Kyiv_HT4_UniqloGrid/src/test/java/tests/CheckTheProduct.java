package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckTheProduct extends BaseTest{

    private static final long DEFAULT_WAITING_TIME = 90;
    private String SEARCHED_PRODUCT = "WOMEN ULTRA LIGHT DOWN VEST";
    private String CHECK_WORDS = "ULTRA LIGHT";
    private int EXPECTED_SEARCH_PRODUCTS_QUANTITY = 9;
    private String EXPECTED_PRODUCT_NAME = "WOMEN ULTRA LIGHT DOWN VEST";

    @Test
    public void searchTheProduct() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().enterTextToSearchField(SEARCHED_PRODUCT);
        getHomePage().implicitWait(60);
        getSearchPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        assertEquals(getSearchPage().getSearchResultsCount(), EXPECTED_SEARCH_PRODUCTS_QUANTITY);
        for (WebElement webElement : getSearchPage().getSearchResultsList()) {
            assertTrue(webElement.getText().contains(CHECK_WORDS));

        }
    }

    @Test
    public void checkThatTheRightProductIsAvailable (){
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().clickWomenSectionButton();
        getWomenPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getWomenPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getWomenPage().getUltraLightShopNow());
        getWomenPage().clickUltraLightShopNow();
        getCoatsJacketsPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getCoatsJacketsPage().clickLightVest();
        getUltraLightVestPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getUltraLightVestPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getUltraLightVestPage().getLightVestProductName());
        assertEquals(getUltraLightVestPage().getLightVestProductNameText(), EXPECTED_PRODUCT_NAME);
        assertTrue(getUltraLightVestPage().isBlackColorButtonVisible());
        assertTrue(getUltraLightVestPage().isSizeMButtonVisible());
        assertTrue(getUltraLightVestPage().isAddToBagButtonVisible());
    }

    @Test
    public void checkThatTheLoginFormIsVisible(){
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().clickLoginButtonMain();
        getLoginPage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        assertTrue(getLoginPage().isCreateAccountFormVisible());
        assertTrue(getLoginPage().isLoginButtonFormVisible());
        assertTrue(getLoginPage().isEmailAddressLineVisible());
        assertTrue(getLoginPage().isEnterPasswordVisible());
        assertTrue(getLoginPage().isLoginButtonVisible());
        assertTrue(getLoginPage().isForgotThePasswordVisible());
    }

}
