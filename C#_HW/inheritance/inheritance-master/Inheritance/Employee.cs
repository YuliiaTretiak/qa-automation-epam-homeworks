using System;

namespace InheritanceTask
{
    class Employee
    {
        protected string name;
        protected decimal salary;
        protected decimal bonus;
        public string Name
        {
            get { return name; }
        }

        public decimal Salary
        {
            get { return salary; }
            set { salary = value; }
        }
        public Employee(string name, decimal salary)
        {
            this.name = name;
            this.salary = salary;
        }
        public virtual void SetBonus (decimal bonus)
        {
            this.bonus = bonus;
        }
        public decimal ToPay()
        {
            var sum = this.bonus + this.salary;
            return sum;
        }
    }
}

