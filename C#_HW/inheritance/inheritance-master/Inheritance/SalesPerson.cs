using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
     class SalesPerson : Employee
    {
        readonly int percent;
        public SalesPerson (string name, decimal salary, int percent) : base (name, salary)
        {
        this.percent = percent;}
        public override void SetBonus(decimal bonus)
        {
            if (this.percent>= 100 && this.percent<=199)
            { 
                base.SetBonus(bonus * 2); }
            else if(this.percent>=200)
            {
                base.SetBonus(bonus * 3);
            }
            else
            {
                base.SetBonus(bonus);
            }
        }
    }
}
