using System;

namespace Function
{
    public enum SortOrder { Ascending, Descending }
    public static class Function
    {
       public static bool IsSorted(int[] array, SortOrder order)
        {
            int prev = array[0];

            for (int i = 1; i < array.Length; i++)
            {
                var current = array[i];
                if (current > prev && order == SortOrder.Ascending)
                {
                    prev = current;
                    continue;
                }
                else if (current < prev && order == SortOrder.Descending)
                {
                    prev = current;
                    continue;
                }

                if (current == prev)
                {
                    prev = current;
                    continue;
                }
                return false;
            }
            return true;

        }

         public static void Transform(int[] array, SortOrder order)
        {
            var isSorted = IsSorted(array, order);
            if (!isSorted)
            {
                return;
            }

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = array[i] + i;
            }
        }

        public static double MultArithmeticElements(double a, double t, int n)
        {
            double result = a;
            for (double i = 1; i < n; i++)
            {
                result *= a + t;
                a = a + t;
            }
            return result;
        }

       public static double SumGeometricElements(double a, double t, double alim)
        {
            bool torepeat = true;
            double result = 0;
            while (torepeat)
            {
                torepeat = a > alim;
                if (!torepeat)
                {
                    break;
                }
                if (result == 0)
                {
                    result = a;
                }
                var currentAdd = a * t;
                if (currentAdd > alim)
                {
                    result += currentAdd;
                }
                a = currentAdd;
            }
            return result;
        }

    }
}
