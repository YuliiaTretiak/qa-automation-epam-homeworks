package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class iPadMiniPage extends BasePage {

    @FindBy(xpath = "//label[@for='fltr-nakopitel-64gb']")
    private WebElement filter64Gb;

    @FindBy(xpath = "//div[@class='prod-cart__descr' and contains(text(),'MK7P3')]")
    private WebElement filterContainsProduct;

    @FindBy(xpath = "//a[@class='tags__item' and contains(text(),'64')]")
    private WebElement filterContains64GB;

    @FindBy(xpath = "//div[@class='prod-cart__descr']")
    private List<WebElement> findIPadMiniProductsListText;

    public iPadMiniPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getIPadMiniList (){ return findIPadMiniProductsListText;}

    public void clickFilter64Gb () {filter64Gb.click();}

    public WebElement getFilterContains64GB (){return filterContains64GB;}

    public void clickFilterContainsProduct (){filterContainsProduct.click();}

}
