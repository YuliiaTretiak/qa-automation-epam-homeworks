package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class SearchPage extends BasePage {

        @FindBy(xpath = "//a[contains(@class,'productTile__link js')]")
        private List<WebElement> searchResultsProductsListText;

        public SearchPage(WebDriver driver) {
            super(driver);
        }

        public List<WebElement> getSearchResultsList() {
            return searchResultsProductsListText;
        }

        public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }
}
