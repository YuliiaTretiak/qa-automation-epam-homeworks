using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class SpecialDeposit : Deposit
    {
        public SpecialDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            var depositIncome = Amount;
            for (int i = 1; i <= Period; i++)
            {
                depositIncome += (depositIncome / 100) * i;
            }
            var income = depositIncome - Amount;
            return income;
        }
        public bool CanToProlong()
        {
            return Amount > 1000;
        }
    }


}
