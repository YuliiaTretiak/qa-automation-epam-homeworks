package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//a[contains(@class,'js_navMenuCategory navMenu') and contains(text(),'Women')]")
    private WebElement womenSectionButton;

    @FindBy(xpath = "//div[contains(@class,'js-userLogin')]")
    private WebElement loginButtonMain;

    @FindBy(xpath = "//input[contains(@class,'search-input')]")
    private WebElement searchField;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickWomenSectionButton () {womenSectionButton.click();}

    public void clickLoginButtonMain () {loginButtonMain.click();}

    public void enterTextToSearchField(final String element) {
        searchField.clear();
        searchField.sendKeys(element, Keys.ENTER);
    }
}
