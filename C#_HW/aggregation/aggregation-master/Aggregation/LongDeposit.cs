namespace Aggregation
{
   class LongDeposit : Deposit
    {
    public LongDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
    {
    }
        public override decimal Income()
        {
            var depositIncome = Amount;
            if (Period > 6)
            {
                for (int i = Period-6; i > 0; i--)
                {
                    var amount1 = depositIncome * (decimal)1.15;
                    depositIncome = amount1;
                }
            }
            var income = depositIncome - Amount;
            return income;
        }
    }
}
