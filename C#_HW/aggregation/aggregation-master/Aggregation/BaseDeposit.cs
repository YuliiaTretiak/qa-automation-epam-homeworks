namespace Aggregation
{
     class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            var depositIncome = Amount;
            
            for (int i = Period; i >0; i--)
            {
                var amount1 = depositIncome * (decimal) 1.05;
                depositIncome = amount1;
            }
            var income = depositIncome - Amount;
            return income;
        }
    }
}
