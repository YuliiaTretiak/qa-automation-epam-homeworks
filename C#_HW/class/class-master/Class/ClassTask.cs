using System;
using System.Collections.Generic;
using System.Linq;

namespace Class
{
  class Rectangle
    {
        private double sideA;
        private double sideB;
        public Rectangle(double a, double b)
        {
            this.sideA = a;
            this.sideB = b;
        }
        public Rectangle(double a)
        {
            this.sideA = a;
            this.sideB = 5;
        }
        public Rectangle()
        {
            this.sideA = 4;
            this.sideB = 3;
        }
        public double GetSideA()
        {
            return this.sideA;
        }
        public double GetSideB()
        {
            return this.sideB;
        }
        public double Area()
        {
            double area = this.sideA * this.sideB;
            return area;
        }
        public double Perimeter()
        {
            return (2 * this.sideA + 2 * this.sideB);
        }
        public bool IsSquare()
        {
            return this.sideA == this.sideB;
        }

        public void ReplaceSides()
        {
            var temp = sideB;
            sideB = sideA;
            sideA = temp;

        }
    }

    class ArrayRectangles
    {
        private Rectangle[] rectangle_array;
        public ArrayRectangles(int n)
        {
            rectangle_array = new Rectangle[n];
        }
        public ArrayRectangles(IEnumerable<Rectangle> arr)
        {
            rectangle_array = arr.ToArray();
        }
        public bool AddRectangle(Rectangle rec)
        {
            if (rectangle_array.Count() == rectangle_array.Length)
            {
                return false;
            }

            rectangle_array.Append(rec);
            return true;
        }
        public int NumberMaxArea()
        {
            var ind = 0;
            double maxArea = 0;

            for (int i = 0; i < rectangle_array.Length; i++)
            {
                var area = rectangle_array[i].Area();
                if (area > maxArea)
                {
                    maxArea = area;
                    ind = i;
                }
            }
            return ind;
        }
        public int NumberMinPerimeter()
        {
            var ind = 0;
            double minPerim = double.MaxValue;

            for (int i = 0; i < rectangle_array.Length; i++)
            {
                var perim = rectangle_array[i].Perimeter();
                if (perim < minPerim)
                {
                    minPerim = perim;
                    ind = i;
                }
            }
            return ind;
        }
        public int NumberSquare()
        {
            var count = 0;
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                var square = rectangle_array[i].IsSquare();
                if (square)
                {
                    count += 1;
                }
            }
            return count;
        }
    } 
}
