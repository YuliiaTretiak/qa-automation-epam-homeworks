package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WomenPage extends BasePage{

    @FindBy(xpath = "//div[@class='cat']//a[@href='https://www.uniqlo.com/uk/en/women/outerwear/coats-jackets']")
    private WebElement ultraLightShopNow;

    public WomenPage(WebDriver driver) {
        super(driver);
    }

    public void clickUltraLightShopNow () {ultraLightShopNow.click();}

    public WebElement getUltraLightShopNow () {return ultraLightShopNow;}


}
