package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage{

    @FindBy(xpath = "//h2[contains(text(),'NOTHING MATCHES')]")
    private WebElement nothingMatchesYourSearch;

    @FindBy(xpath = "//section//article//div/h2")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//article[@data-auto-id='productTile']")
    private List<WebElement> amountOfProductsPage;

    @FindBy(xpath = "//section//article[1]")
    private WebElement theFirstDress;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isNothingMatchesYourSearchVisible() {
        return nothingMatchesYourSearch.isDisplayed();
    }

    public List<WebElement> getSearchResultsList() {
        return amountOfProductsPage;
    }

    public int getAmountOfProductsPage() {
        return getSearchResultsList().size();
    }

    public void clickTheFistDress() { theFirstDress.click();}
}
