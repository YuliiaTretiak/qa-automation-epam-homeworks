package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(driver);
    }

    public AddToCartPage getAddToCartPage() {
        return new AddToCartPage(driver);
    }

    public WomenPage getWomenPage() { return new WomenPage(driver);}

    public ActivewarePage getActivewarePage() { return new ActivewarePage(driver);}

    public FirstDressPage geFistDressPage() {return  new FirstDressPage(driver);}

    public MyAccountPage getMyAccountPage() { return  new MyAccountPage(driver);}
}
