using System;
using System.Collections.Generic;

namespace Extension
{
    public static class MyExtension
    {
        /// <summary>
        /// Mehod that return sum of  'n' digit
        /// </summary>        
        /// <param name="n">Element parameter</param>
        /// <returns>Integer value</returns>
        public static int SummaDigit(this int n)
        {
            if(n<0)
            {
                n *= -1;
            }
            var sum = 0;
            for (int i = 0; n/10>=1; i++)
            {
                sum += n % 10;
                n/=10;
            }
            sum += n;
            return sum;
        }
       
        /// <summary>
        /// Method that return sum of 'n' element and reverse of 'n'
        /// </summary>
        /// <param name="n">Element parameter</param>
        /// <returns>Ulong value</returns>
        public static ulong SummaWithReverse(this uint n)
        {
            uint x = n;
            uint reverse = 0;
            for (int i = 0; x/10>=1; i++)
            {
                reverse = (reverse+x % 10)*10;
                x /= 10;
            }
            reverse += x;
            return reverse + n;
        }
       
        /// <summary>
        /// Method that count amount of elements in string , which are not letters of the latin alphabet.
        /// </summary>
        /// <param name="str">String parameter</param>
        /// <returns>Integer value</returns>
        public static int CountNotLetter(this string str)
        {
            int sum = str.Length;
            foreach (char c in str)
            {
                if ((c>= 'a' && c<='z')|| (c>= 'A' && c<='Z'))
                { 
                    sum --;
                }
            }
            return sum;
        }

      
        /// <summary>
        /// Method that checks whether day is weekend or not 
        /// </summary>
        /// <param name="day">DayOfWeek parameter</param>
        /// <returns>Bool value</returns>
        public static bool IsDayOff(this DayOfWeek day)
        {
            if (day ==DayOfWeek.Saturday || day == DayOfWeek.Sunday)
            {
                return true;
            }
           else { return false; }
        }


        /// <summary>
        /// Method that return positive ,even  element from collection 
        /// </summary>
        /// <param name="numbers">Collection of elements</param>
        /// <returns>IEnumerable -int collection  </returns>
        public static IEnumerable<int> EvenPositiveElements(this IEnumerable<int> numbers)
        {
            foreach (var element in numbers)
            {
                if (element % 2 == 0 && element>0)
                {
                    yield return element;
                }
            }
        }
    }
}
