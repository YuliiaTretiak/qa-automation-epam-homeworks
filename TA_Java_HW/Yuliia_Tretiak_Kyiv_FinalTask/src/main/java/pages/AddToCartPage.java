package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddToCartPage extends BasePage{

    @FindBy(xpath = "//div[contains(@class,'bag-item-descriptions')]")
    private WebElement cartDescription;

    @FindBy(xpath = "//span[@aria-label='Size']//span[@title]")
    private WebElement sizeDescription;

    public AddToCartPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getCartDescription() {return cartDescription;}

    public String getSizeDescriptionText() { return sizeDescription.getText();}

}
