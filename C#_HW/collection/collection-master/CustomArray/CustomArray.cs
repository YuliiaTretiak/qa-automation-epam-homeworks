using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        private readonly T[] arr;
        private int firstIndex;

        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First
        {
            get => firstIndex;
        }

        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last
        {
            get => First + Length - 1;
        }

        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>
        public int Length
        {
            get => arr.Length;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("The value is smaller than 0");
                }
            }
        }

        /// <summary>
        /// Should return array 
        /// </summary>
        public T[] Array
        {
            get => arr;
        }

        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>      
        public CustomArray(int first, int length)
        {
            if (length <= 0)
            {
                throw new ArgumentException("Count is smaller than 0");
            }
            firstIndex = first;
            arr = new T[length];
        }

        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="NullReferenceException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when count is smaler than 0</exception>       
        public CustomArray(int first, IEnumerable<T> list)
        {
            firstIndex = first;
            if (list == null)
            {
                throw new NullReferenceException("This list is null");
            }
            if ((list as List<T>).Count <= 0)
            {
                throw new ArgumentException("Count is smaller than 0");
            }
            arr = (list as List<T>).ToArray();
        }

        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when list without elements </exception>
        public CustomArray(int first, params T[] list)
        {
            firstIndex = first;
            if (list == null)
            {
                throw new ArgumentNullException("The list is null");
            }
            if (list.Length <= 0)
            {
                throw new ArgumentException("The list is without elements");
            }
            arr = list;
        }

        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when index out of array range</exception>  
        /// <exception cref="ArgumentNullException">Thrown in set  when value passed in indexer is null</exception>       
        public T this[int item]
        {
            get
            {
                if (item <= First || item >= First + Length)
                {
                    throw new ArgumentException();
                }

                return arr[item - First];
            }
            set
            {
                if (item <= First || item >= First + Length)
                {
                    throw new ArgumentException();
                }
                if (value == null)
                {
                    throw new ArgumentNullException();
                }
                arr[item - First] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return arr.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return arr.GetEnumerator();
        } 
    }
}
