package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FirstDressPage extends BasePage {

    Actions action;

    @FindBy(xpath = "//button[contains(@data-bind,'addToBag')]")
    private WebElement addToBagButton;

    @FindBy(xpath = "//span[@class='error basic-error-box']")
    private WebElement errorMessage;

    @FindBy(xpath = "//select[@data-id='sizeSelect']")
    private WebElement sizeDropDownMenu;

    @FindBy(xpath = "//select[@data-id='sizeSelect']/option")
    private List<WebElement> sizeOptions;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement bagIcon;

    @FindBy(xpath = "//a[@data-test-id='bag-link']")
    private WebElement viewBagButton;

    @FindBy(xpath = "//a[@data-test-id='checkout-link']")
    private WebElement checkoutButton;

    public FirstDressPage(WebDriver driver) {
        super(driver);
        action = new Actions(this.driver);
    }

    public WebElement getAddToBagButton() {
        return addToBagButton;
    }

    public boolean isAddToBagButtonVisible() {
        return addToBagButton.isDisplayed();
    }

    public void clickAddToBagButton() {
        addToBagButton.click();
    }

    public String errorMessageAddToCart() {
        return errorMessage.getText();
    }

    public WebElement getErrorMessage() {
        return errorMessage;
    }

    public boolean isSizeDropDownMenuVisible() {
        return sizeDropDownMenu.isDisplayed();
    }

    public void clickSizeDropDownMenu() {
        sizeDropDownMenu.click();
    }

    public void clickSizeOptions(final String size) {
        for (WebElement item : sizeOptions) {
            if (item.getText().contains(size)) {
                item.click();
            }
        }
    }

    public void hoverBagIcon() {
        action.moveToElement(bagIcon).perform();
   }

    public boolean isViewBagButtonVisible() {
        return viewBagButton.isDisplayed();
    }

    public boolean isCheckoutButtonVisible() {
        return checkoutButton.isDisplayed();
    }

    public void clickViewBagButton() {
        viewBagButton.click();
    }

    public WebElement getBagIcon() { return bagIcon;}

}
