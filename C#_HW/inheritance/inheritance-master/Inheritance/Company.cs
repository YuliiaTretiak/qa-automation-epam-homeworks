using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{
    class Company
    {
        readonly Employee[] employees;
        public Company(Employee[] employees)
        {
            this.employees = employees;
        }
        public void GiveEverybodyBonus(decimal companyBonus)
        {
            foreach (var employees in employees)
            {
                employees.SetBonus(companyBonus);
            }
        }
        public decimal TotalToPay()
        {
            decimal toPay = 0;
            foreach (var employees in employees)
            {
                toPay += employees.ToPay();
            }
            return toPay;
        }
        public string NameMaxSalary()
        {
            string name = "";
            decimal maxPay = 0;
            foreach (Employee employee in employees)
            {
                decimal pay = employee.ToPay();
                if (pay > maxPay)
                {
                    name = employee.Name;
                    maxPay = pay;
                }
            }
            return name;
        }
    }
}
