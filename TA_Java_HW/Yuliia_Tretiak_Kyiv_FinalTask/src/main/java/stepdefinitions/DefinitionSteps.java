package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 120;
    private String EXPECTED_FLAG = "Ukraine";
    private String EXPECTED_QUANTITY_OF_PRODUCTS = "1";

    WebDriver driver;
    HomePage homePage;
    AddToCartPage addToCartPage;
    SearchResultsPage searchResultsPage;
    PageFactoryManager pageFactoryManager;
    WomenPage womenPage;
    ActivewarePage activewarePage;
    FirstDressPage firstDressPage;
    MyAccountPage myAccountPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks the header visibility")
    public void checkHeaderVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isHeaderVisible());
    }

    @And("User checks the footer visibility")
    public void checkFooterVisibility() {
        assertTrue(homePage.isFooterVisible());
    }

    @And("User checks the search field visibility")
    public void checkSearchFieldVisibility() {
        assertTrue(homePage.isSearchFieldVisible());
    }

    @And("User checks the women section button visibility")
    public void checkWomenSectionButtonVisibility() {
        assertTrue(homePage.isWomenButtonVisible());
    }

    @And("User checks the men section button  visibility")
    public void checksMenSectionButtonVisibility() {
        assertTrue(homePage.isMenButtonVisible());
    }

    @And("User checks account icon visibility")
    public void userChecksAccountIconVisibility() {
        assertTrue(homePage.isAccountIconVisible());
    }

    @And("User checks saved items icon visibility")
    public void userChecksSavedItemsIconVisibility() {
        assertTrue(homePage.isSavedItemsIconVisible());
    }

    @And("User checks bag icon visibility")
    public void userChecksBagIconVisibility() {
        assertTrue(homePage.isCartIconVisible());
    }

    @And("User checks country selector visibility")
    public void userChecksCountrySelectorVisibility() {
        assertTrue(homePage.isCountrySelectorButtonVisible());
    }

    @And("User clicks country selector button")
    public void userClicksCountrySelectorButton() {
        homePage.clickCountrySelector();
    }

    @And("User checks country selector pop-up visibility")
    public void userChecksCountrySelectorPopUpVisibility() {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCountrySelectorForm());
        assertTrue(homePage.isCountrySelectorFormVisible());
    }

    @And("User checks country selector drop down menu visibility")
    public void userChecksCountrySelectorDropDownMenuVisibility() {
        assertTrue(homePage.isCountrySelectorDropdownMenuVisible());
    }

    @And("User clicks country selector drop down menu")
    public void userClicksCountrySelectorDropDownMenu() {
        homePage.clickCountrySelectorDropdownMenu();
    }

    @And("User checks country {string} item visibility")
    public void userChecksCountryVisibility(final String country) {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isCountryVisible(country));
    }

    @And("User clicks country {string} item")
    public void userClicksCountryItem(final String country) {
        homePage.clickCountry(country);
    }

    @And("User checks currency selector drop down menu visibility")
    public void userChecksCurrencySelectorVisibility() {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isCurrencySelectorDropdownMenuVisible());
    }

    @And("User clicks currency selector drop down menu")
    public void userClicksCurrencySelector() {
        homePage.clickCurrencySelectorDropdownMenu();
    }

    @And("User clicks currency {string} item")
    public void userClicksCurrencyItem(final String currency) {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.clickCurrency(currency);
    }

    @And("User checks update preference button visibility")
    public void userChecksUpdatePreferenceButtonVisibility() {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isUpdatePreferenceButtonVisible());
    }

    @And("User clicks update preference button")
    public void userClicksUpdatePreferenceButton() {
        homePage.clickUpdatePreferenceButton();
    }

    @And("User checks country flag {string}")
    public void userChecksCountryFlag(final String flag) {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.checkCountryFlag(flag));
    }

    @And("User enters {string} into search field")
    public void userEntersKeywordIntoSearchField(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void userClicksSearchButton() {
        homePage.clickSearchButton();
    }

    @And("User checks that message saying that no results is shown")
    public void userChecksErrorResultMessage() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(searchResultsPage.isNothingMatchesYourSearchVisible());
    }

    @And("User checks that result contains {string}")
    public void userChecksThatResultContainsKeyword(final String amountOfProducts) {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertEquals(Integer.parseInt(amountOfProducts), searchResultsPage.getAmountOfProductsPage());
        }

    @And("User checks that amount of products on a search page are {string}")
    public void userChecksAmountOfProducts(final String amountOfProducts) {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertEquals(Integer.parseInt(amountOfProducts), searchResultsPage.getAmountOfProductsPage());
    }

    @And("User clicks the women section button")
    public void userClicksTheWomenSectionButton() {
        homePage.clickWomenButton();
    }

    @And("User checks Trending now filter buttons visibility")
    public void userChecksTrendingNowFilterButtonsVisibility() {
        womenPage = pageFactoryManager.getWomenPage();
        womenPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        womenPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(womenPage.isTrendingNowButtonVisible());
    }

    @And("User clicks Activeware in Tending now filter section")
    public void userClicksActivewareInTendingNowFilterSection() {
        womenPage.hoverTrendingNowButton();
        womenPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,womenPage.getActiveware());
        womenPage.clickActiveware();
    }

    @And("User checks Activity drop down menu buttons visibility")
    public void userChecksActivityDropDownMenuButtonsVisibility() {
        activewarePage = pageFactoryManager.getActivewarePage();
        activewarePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        activewarePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(activewarePage.isActivityDropdownMenuButtonVisible());
    }

    @And("User clicks Activity drop down menu button")
    public void userClicksActivityDropDownMenuButton() {
        activewarePage.clickActivityDropdownMenuButton();
    }

    @And("User checks {string} activity button visibility")
    public void userChecksActivityButtonVisibility(final String activity) {
        activewarePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(activewarePage.isActivityButtonVisible(activity));
    }

    @And("User clicks {string} activity button")
    public void userClicksActivityActivityButton(final String activity) {
        activewarePage.clickActivityButton(activity);
    }

    @And("User checks Color drop down menu buttons visibility")
    public void userChecksColorDropDownMenuButtonsVisibility() {
        activewarePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(activewarePage.isColorDropdownMenuButtonVisible());
    }

    @And("User clicks Color drop down menu button")
    public void userClicksColorDropDownMenuButton() {
        activewarePage.clickColorDropdownMenuButton();
    }

    @And("User checks {string} color button visibility")
    public void userChecksColorButtonVisibility(final String color) {
        activewarePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(activewarePage.isColorButtonVisible(color));
    }

    @And("User clicks {string} color button")
    public void userClicksColorButton(final String color) {
        activewarePage.clickColorButton(color);
    }

    @And("User checks that amount of products are {string}")
    public void checkAmountOfProducts(final String amountOfProducts) {
        activewarePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, activewarePage.getProductList());
        assertEquals(Integer.parseInt(amountOfProducts), activewarePage.getAmountOfChosenProducts());
    }

    @And("User clicks on the first product")
    public void userClicksOnTheFirstProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.clickTheFistDress();
    }

    @And("User checks Add to bag button visibility")
    public void userChecksAddToBagButtonVisibility() {
        firstDressPage = pageFactoryManager.geFistDressPage();
        firstDressPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        firstDressPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        firstDressPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, firstDressPage.getAddToBagButton());
        assertTrue(firstDressPage.isAddToBagButtonVisible());
    }

    @And("User clicks Add to bag button on product")
    public void userClicksAddToBagButtonOnProduct() {
        firstDressPage.clickAddToBagButton();
        firstDressPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        firstDressPage.waitForAjaxToCompletePdp(DEFAULT_TIMEOUT);
    }

    @And("User checks the {string}")
    public void userChecksTheMessage(final String errorMessage) {
        firstDressPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, firstDressPage.getErrorMessage());
        assertTrue(firstDressPage.errorMessageAddToCart().contains(errorMessage));
    }

    @And("User checks size drop down menu visibility")
    public void userChecksSizeDropDownMenuVisibility() {
        assertTrue(firstDressPage.isSizeDropDownMenuVisible());
    }

    @And("User clicks size drop down menu")
    public void userClicksSizeDropDownMenu() {
        firstDressPage.clickSizeDropDownMenu();
    }

    @And("User clicks on {string} size")
    public void userClicksOnSize(final String size) {
        firstDressPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        firstDressPage.clickSizeOptions(size);
    }

    @And("User checks view bag and check out buttons visibility")
    public void userChecksBagDropDownVisibility() {
        firstDressPage.waitForTextToBePresented(DEFAULT_TIMEOUT,firstDressPage.getBagIcon(), "1");
        firstDressPage.hoverBagIcon();
        firstDressPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(firstDressPage.isViewBagButtonVisible());
        assertTrue(firstDressPage.isCheckoutButtonVisible());
    }

    @And("User clicks MyAccount button")
    public void userClicksMyAccountButton() {
        homePage.clickAccountIcon();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getMyAccountButton());
        homePage.clickMyAccountButton();
    }

    @And("User checks sign in email address field visibility")
    public void userChecksSignInEmailAddressFieldVisibility() {
        myAccountPage = pageFactoryManager.getMyAccountPage();
        myAccountPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        myAccountPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,  myAccountPage.getSignInButton());
        assertTrue(myAccountPage.isSignInEmailFormVisible());
    }

    @And("User checks sign in password field visibility")
    public void userChecksSignInPasswordFieldVisibility() {
        assertTrue(myAccountPage.isSighnInPasswordFormVisible());
    }

    @And("User checks sign in button visibility")
    public void userChecksSignInButtonVisibility() {
        assertTrue(myAccountPage.isSignInButtonVisible());
    }

    @And("User checks Forgot password button visibility")
    public void userChecksForgotPasswordButtonVisibility() {
        assertTrue(myAccountPage.isForgotPasswordButtonVisible());
    }

    @And("User clicks on Join button")
    public void userClicksOnJoinButton() {
        myAccountPage.clickJoinButton();
    }

    @And("User checks Join email address field visibility")
    public void userChecksJoinEmailAddressFieldVisibility() {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getJoinAsosButton());
        assertTrue(myAccountPage.isJoinEmailFieldVisible());
    }

    @And("User checks Join first name field visibility")
    public void userChecksJoinFirstNameFieldVisibility() {
        assertTrue(myAccountPage.isJoinFirstNameFieldVisible());
    }

    @And("User checks Join last name field visibility")
    public void userChecksJoinLastNameFieldVisibility() {
        assertTrue(myAccountPage.isJoinLastNameFieldVisible());
    }

    @And("User checks Join password field visibility")
    public void userChecksJoinPasswordFieldVisibility() {
        assertTrue(myAccountPage.isJoinPasswordFieldVisible());
    }

    @And("User checks date of birth section visibility")
    public void userChecksDateOfBirthSectionVisibility() {
        assertTrue(myAccountPage.isBirthDateFieldVisible());
    }

    @And("User checks Join Asos button visibility")
    public void userChecksJoinAsosButtonVisibility() {
        assertTrue(myAccountPage.isJoinAsosButtonVisible());
    }

    @And("User enters {string} into email field")
    public void userEntersIncorrectEmail(final String incorrectEmail) {
        myAccountPage = pageFactoryManager.getMyAccountPage();
        myAccountPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        myAccountPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,  myAccountPage.getSignInButton());
        myAccountPage.enterIncorrectEmail(incorrectEmail);
    }

    @And("User enters {string} into password field")
    public void userEntersIncorrectPasswordIn(final String incorrectPassword) {
        myAccountPage.enterIncorrectPassword(incorrectPassword);
    }

    @And("User clicks Sign In button")
    public void userClicksSignInButton() {
        myAccountPage.clickSighInButton();
    }

    @And("User checks {string} error message visibility")
    public void userChecksSignInErrorMessageVisibility(final String signInErrorMessage) {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getErrorEmailMessage());
        assertTrue(myAccountPage.getErrorEmailMessageText().contains(signInErrorMessage));
    }

    @And("User click Forgot password button")
    public void userClickForgotPasswordButton() {
        myAccountPage.clickForgotPasswordButton();
    }

    @And("User enters {string} for validation")
    public void userEntersIncorrectEmailForValidation(final String incorrectEmail ) {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getResetPasswordButton());
        myAccountPage.enterValidationEmail(incorrectEmail );
    }

    @And("User clicks Reset password")
    public void userClicksResetPassword() {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getSignInResentPassButton());
        myAccountPage.clickResetPasswordButton();
    }

    @And("User checks {string} visibility")
    public void userChecksFinalMessageVisibility(final String finalMessage) {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getResentPasswordMessage());
        assertTrue(myAccountPage.getResentPasswordMessageText().contains(finalMessage));
    }

    @And("User checks error message visibility")
    public void userChecksErrorMessageVisibility() {
        myAccountPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, myAccountPage.getErrorEmailMessage());
        assertTrue(myAccountPage.isErrorEmailMessageVisible());
    }

    @And("User clicks View bag button")
    public void userClicksViewBagButton() {
        firstDressPage.implicitWait(30);
        firstDressPage.clickViewBagButton();
    }

    @And("User checks the size {string}")
    public void userChecksTheSizeSize(final String size) {
        addToCartPage = pageFactoryManager.getAddToCartPage();
        addToCartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        addToCartPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        addToCartPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,  addToCartPage.getCartDescription());
        assertTrue(addToCartPage.getSizeDescriptionText().contains(size));
    }

}

