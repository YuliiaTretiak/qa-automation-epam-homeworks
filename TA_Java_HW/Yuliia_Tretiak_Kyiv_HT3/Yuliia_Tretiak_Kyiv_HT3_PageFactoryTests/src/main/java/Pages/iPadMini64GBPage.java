package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class iPadMini64GBPage extends BasePage{

    @FindBy(xpath = "//div[@class='total-h']//span[@class='prise']")
    private WebElement cartPrice;

    @FindBy(xpath = "//input[@class='js-changeAmount count']")
    private WebElement quantity;

    @FindBy(xpath = "//span[@id='total-price']")
    private WebElement sum;

    @FindBy(xpath = "//a[@class='main-btn main-btn--green main-btn--large']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[@class='js_plus btn-count btn-count--plus ']")
    private WebElement plusQuantityButton;

    @FindBy(id = "js_cart")
    private WebElement addToCartPopup;

    @FindBy(xpath = "//span[@class='name']")
    private WebElement productName;

    @FindBy(xpath = "//a[@href ='https://avic.ua/checkout']")
    private WebElement orderCheckout;

    public iPadMini64GBPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddToCartButton (){addToCartButton.click();}

    public WebElement getAddToCartPopup() {
        return addToCartPopup;
    }

    public String getProductName() { return productName.getText();}

    public String getQuantity() {return quantity.getText();}

    public String getCartPrice() {return cartPrice.getText();}

}
