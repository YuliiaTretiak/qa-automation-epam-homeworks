package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//li[@class='parent js_sidebar-item']//a[@href='https://avic.ua/apple-store']")
    private WebElement appleStoreButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickAppleStoreButton() {appleStoreButton.click();}

}

