Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

Scenario Outline: Check the main elements on the Home Page
  Given User opens '<homePage>' page
  And User checks the header visibility
  And User checks the footer visibility
  And User checks the search field visibility
  And User checks the women section button visibility
  And User checks the men section button  visibility
  And User checks account icon visibility
  And User checks saved items icon visibility
  And User checks bag icon visibility
  And User checks country selector visibility
  When User clicks country selector button
  Then User checks country selector pop-up visibility
  And User checks country selector drop down menu visibility
  When User clicks country selector drop down menu
  Then User checks country '<country>' item visibility
  And User clicks country '<country>' item
  And User checks currency selector drop down menu visibility
  And User clicks currency selector drop down menu
  And User clicks currency '<currency>' item
  And User checks update preference button visibility
  When User clicks update preference button
  Then User checks country flag '<countryFlag>'

  Examples:
    | homePage              | country | currency | countryFlag |
    | https://www.asos.com/ | UA      | USD      | Ukraine     |

Scenario Outline: Check the message if entering an incorrect request in search field
  Given User opens '<homePage>' page
  And User checks the search field visibility
  Then User enters '<keyword>' into search field
  Then User clicks search button
  And User checks that message saying that no results is shown

  Examples:
    | homePage              | keyword  |
    | https://www.asos.com/ | blablablabbllaabbb |

  Scenario Outline: Check search result with correct keyword
    Given User opens '<homePage>' page
    And User checks the search field visibility
    Then User enters '<keyword>' into search field
    Then User clicks search button
    And User checks that amount of products on a search page are '<amountOfProducts>'

    Examples:
      | homePage              | keyword | amountOfProducts |
      | https://www.asos.com/ | socks   | 72               |

    Scenario Outline: Check filters and sorting
      Given User opens '<homePage>' page
      And User checks the women section button visibility
      When User clicks the women section button
      Then User checks Trending now filter buttons visibility
      And User clicks Activeware in Tending now filter section
      And User checks Activity drop down menu buttons visibility
      Then User clicks Activity drop down menu button
      And User checks '<activity>' activity button visibility
      And User clicks '<activity>' activity button
      Then User checks Color drop down menu buttons visibility
      And User clicks Color drop down menu button
      And User checks '<color>' color button visibility
      And User clicks '<color>' color button
      Then User checks that amount of products are '<amountOfProducts>'

      Examples:
        | homePage              | activity | color  | amountOfProducts |
        | https://www.asos.com/ | Running  | Blue   | 31               |

  Scenario Outline: Check that it is impossible to add the product to the cart without size
    Given User opens '<homePage>' page
    And User checks the search field visibility
    When User enters '<keyword>' into search field
    And User clicks search button
    And User clicks on the first product
    And User checks Add to bag button visibility
    And User clicks Add to bag button on product
    Then User checks the '<message>'

    Examples:
      | homePage              | keyword    | message                                                  |
      | https://www.asos.com/ | dress      | Please select from the available colour and size options |

  Scenario Outline: Check add to cart
    Given User opens '<homePage>' page
    And User checks the search field visibility
    When User enters '<keyword>' into search field
    And User clicks search button
    And User clicks on the first product
    And User checks Add to bag button visibility
    And User clicks Add to bag button on product
    And User checks size drop down menu visibility
    And User clicks size drop down menu
    And User clicks on '<size>' size
    And User checks Add to bag button visibility
    And User clicks Add to bag button on product
    And User checks view bag and check out buttons visibility

    Examples:
      | homePage              | keyword | size  |
      | https://www.asos.com/ | dress   | EU 44 |

  Scenario Outline: Check Add to cart page
    Given User opens '<homePage>' page
    And User checks the search field visibility
    When User enters '<keyword>' into search field
    And User clicks search button
    And User clicks on the first product
    And User checks Add to bag button visibility
    And User clicks Add to bag button on product
    And User checks size drop down menu visibility
    And User clicks size drop down menu
    And User clicks on '<size>' size
    And User checks Add to bag button visibility
    And User clicks Add to bag button on product
    Then User clicks View bag button
    And User checks the size '<size>'

    Examples:
      | homePage              | keyword | size  |
      | https://www.asos.com/ | dress   | EU 44 |

  Scenario Outline: Check MyAccount main fields
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    Then User checks sign in email address field visibility
    And User checks sign in password field visibility
    And User checks sign in button visibility
    And User checks Forgot password button visibility
    Then User clicks on Join button
    And User checks Join email address field visibility
    And User checks Join first name field visibility
    And User checks Join last name field visibility
    And User checks Join password field visibility
    And User checks date of birth section visibility
    And User checks Join Asos button visibility

    Examples:
      | homePage              |
      | https://www.asos.com/ |

  Scenario Outline: Check sign in while entering incorrect email and password
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    Then User enters '<incorrectEmail>' into email field
    Then User enters '<incorrectPassword>' into password field
    And User clicks Sign In button
    And User checks '<signInErrorMessage>' error message visibility

    Examples:
      | homePage              | incorrectEmail             | incorrectPassword | signInErrorMessage                                                                |
      | https://www.asos.com/ | yulyallleeetrtteeers@gmail.com | passpass          | Looks like either your email address or password were incorrect. Wanna try again? |

  Scenario Outline: Check forgot password
    Given User opens '<homePage>' page
    And User clicks MyAccount button
    Then User enters '<incorrectEmail>' into email field
    Then User enters '<incorrectPassword>' into password field
    And User clicks Sign In button
    And User checks error message visibility
    And User click Forgot password button
    And User enters '<incorrectEmail>' for validation
    And User checks '<finalMessage>' visibility


    Examples:
      | homePage              | incorrectEmail                | incorrectPassword | finalMessage             |
      | https://www.asos.com/ | yulyyaalettrtteeerssss@gmail.com | passpass          | RESET PASSWORD LINK SENT |



