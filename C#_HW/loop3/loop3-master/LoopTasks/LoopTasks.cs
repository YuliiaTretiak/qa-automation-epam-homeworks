using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static int SumOfOddDigits(int n)
        {
          int result = 0;
            while (n > 0)
            {
                int digit = n % 10;
                if (digit % 2 != 0)
                {
                    result += digit;
                }
                n /= 10;
            }
            return result;
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int NumberOfUnitsInBinaryRecord(int n)
        {
         int result = 0;
            for (int i = n; i > 0; i /= 2)
            {
                result += i % 2;
            }
            return result;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int result = 0;
            int one = 0;
            int two = 1;
            for (int i = 1; i < n; i++)
            {
                if (n == 1)
                {
                    result = one;
                }
                else 
                {
                    int sum = one + two;
                    result += two;
                    one = two;
                    two = sum;
                }
            }
            return result;
        }
    }
}
