package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyAccountPage extends BasePage{

    @FindBy(xpath = "//input[contains(@class,'email')]")
    private WebElement signInEmailForm;

    @FindBy(xpath = "//input[contains(@class,'password')]")
    private WebElement signInPasswordForm;

    @FindBy(xpath = "//input[@value='Sign in']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[contains(@class,'forgot-password')]")
    private WebElement forgotPasswordButton;

    @FindBy(xpath = "//a[contains(@class,'qa-join-asos')]")
    private WebElement joinButton;

    @FindBy(xpath = "//input[@alt='Email']")
    private WebElement joinEmailField;

    @FindBy(xpath = "//input[contains(@class,'firstname')]")
    private WebElement joinFirstNameField;

    @FindBy(xpath = "//input[contains(@class,'lastname')]")
    private WebElement joinLastNameField;

    @FindBy(xpath = "//input[contains(@class,'password')]")
    private WebElement joinPasswordField;

    @FindBy(xpath = "//div[@class='field js-birthday ']")
    private WebElement joinBirthDateField;

    @FindBy(xpath = "//input[@value='Join ASOS']")
    private WebElement joinAsosButton;

    @FindBy(xpath = "//li[@id='loginErrorMessage']")
    public WebElement errorEmailMessage;

    @FindBy(xpath = "//input[contains(@class,'email-textbox')]")
    public WebElement validationEmail;

    @FindBy(xpath = "//input[contains(@value,'Reset Password')]")
    public WebElement resetPasswordButton;

    @FindBy(xpath = "//span[@class='qa-title']")
    public WebElement resentPasswordMessage;

    @FindBy(xpath = "//a[@id='sign-in-button']")
    public WebElement signInResentPassButton;

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getSignInButton() {return signInButton;}

    public WebElement getJoinAsosButton() { return joinAsosButton;}

    public boolean isSignInEmailFormVisible() { return signInEmailForm.isDisplayed();}

    public boolean isSighnInPasswordFormVisible() { return signInPasswordForm.isDisplayed();}

    public boolean isSignInButtonVisible() { return signInButton.isDisplayed();}

    public boolean isForgotPasswordButtonVisible() { return forgotPasswordButton.isDisplayed();}

    public  boolean isJoinEmailFieldVisible() { return joinEmailField.isDisplayed();}

    public boolean isJoinFirstNameFieldVisible() { return joinFirstNameField.isDisplayed();}

    public boolean isJoinLastNameFieldVisible() {return joinLastNameField.isDisplayed();}

    public boolean isJoinPasswordFieldVisible() { return joinPasswordField.isDisplayed();}

    public boolean isBirthDateFieldVisible() { return joinBirthDateField.isDisplayed();}

    public boolean isJoinAsosButtonVisible() { return joinAsosButton.isDisplayed();}

    public void clickJoinButton() { joinButton.click();}

    public void enterIncorrectEmail(final String incorrectEmail) {
        signInEmailForm.clear();
        signInEmailForm.sendKeys(incorrectEmail);
        signInEmailForm.sendKeys(Keys.ENTER);
    }

    public void enterIncorrectPassword(final String incorrectPassword) {
        signInPasswordForm.clear();
        signInPasswordForm.sendKeys(incorrectPassword);
    }

    public WebElement getErrorEmailMessage() {return errorEmailMessage;}

    public String getErrorEmailMessageText() { return errorEmailMessage.getText();}

    public void clickSighInButton() { signInButton.click();}

    public boolean isErrorEmailMessageVisible() { return errorEmailMessage.isDisplayed();}

    public void clickForgotPasswordButton() { forgotPasswordButton.click();}

    public void enterValidationEmail(final String incorrectEmail) {
        signInEmailForm.clear();
        signInEmailForm.sendKeys(incorrectEmail);
        signInEmailForm.sendKeys(Keys.ENTER);
    }

    public WebElement getResetPasswordButton() {return resetPasswordButton;}

    public void clickResetPasswordButton() { resetPasswordButton.click();}

    public WebElement getResentPasswordMessage() {return resentPasswordMessage;}

    public String getResentPasswordMessageText() { return resentPasswordMessage.getText();}

    public WebElement getSignInResentPassButton() { return signInResentPassButton;}
}
