import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AvicTestsTretiak {
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeTest
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

    @Test
    public void checkThatChosenCategoryContainsRightProducts() {
        driver.findElement(By.xpath("//li[@class='parent js_sidebar-item']//a[@href='https://avic.ua/apple-store']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//a[@href='https://avic.ua/ipad/seriya--ipad-mini-2021']")).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        List<WebElement> elementList = driver.findElements(xpath("//div[@class='prod-cart__descr']"));
        for (WebElement webElement : elementList){
            assertTrue(webElement.getText().contains("iPad Mini"));
        }
    }

    @Test(dependsOnMethods    = "checkThatChosenCategoryContainsRightProducts")
    public void addToCart (){
        driver.findElement(By.xpath("//label[@for='fltr-nakopitel-64gb']")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='tags__item' and contains(text(),'64')]")));
        driver.findElement(By.xpath("//div[@class='prod-cart__descr' and contains(text(),'MK7P3')]")).click();
        new WebDriverWait(driver, 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        driver.findElement(By.xpath("//a[@class='main-btn main-btn--green main-btn--large']")).click();
        new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='js_cart']")));
    }

    @Test(dependsOnMethods   = "addToCart")
    public void checkTheSum(){
        String priceText = driver.findElement(By.xpath("//div[@class='total-h']//span[@class='prise']")).getText();
        int price = Integer.parseInt(priceText.split(" ")[0]);
        String sumText = driver.findElement(xpath("//div[@class='item-total']//span[@class='prise']")).getText();
        int sum = Integer.parseInt(sumText.split(" ")[0]);
        assertEquals(sum, price);
    }
    @AfterTest
    public void tearDown() {
       driver.close();
    }
}
