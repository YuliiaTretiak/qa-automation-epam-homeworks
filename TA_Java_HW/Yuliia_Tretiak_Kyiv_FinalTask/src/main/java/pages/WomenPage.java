package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class WomenPage extends BasePage{

    Actions action;

    @FindBy(xpath = "//div//button[contains(@data-id,'aea994d09a8c')]")
    public WebElement trendingNowButton;

    @FindBy(xpath = "//a[contains(@href,'most+wanted|activewear')]")
    public WebElement activeware;

    public WomenPage(WebDriver driver) {
        super(driver);
        action = new Actions(this.driver);
    }

    public void hoverTrendingNowButton() {
        action.moveToElement(trendingNowButton).perform();
    }

    public void clickActiveware () { activeware.click();}

    public boolean isTrendingNowButtonVisible() { return trendingNowButton.isDisplayed();}

    public WebElement getActiveware() {return activeware;}
}
