using System;
using System.Collections.Generic;
using System.Linq;

namespace Condition
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
        {
            int result = 0;
            if (n > 0)
            {
                result = n * n;
            }
            else if (n < 0)
            {
                result = Math.Abs(n);
            }
            return result;
        }

        /// <summary>
        /// Implement code according to description of  task 2
        /// </summary>  
        public static int Task2(int n)
        {
            int n0 = n % 10;
            int n1 = (n % 100 - n0) / 10;
            int n2 = n / 100;

            var arr = new List<int> { n0, n1, n2 }.OrderByDescending(i => i).ToArray();

            var result = arr[0] * 100 + arr[1] * 10 + arr[2];
            return result;
        }
    }
}
