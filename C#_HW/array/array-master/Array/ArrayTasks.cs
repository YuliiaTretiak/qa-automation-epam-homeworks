using System;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static void ChangeElementsInArray(int[] nums)
        {
            int j = nums.Length - 1;
            if (nums.Length != 0 || nums.Length != 1)
            {
                for (int i = 0; i < nums.Length / 2; i++)
                {
                    int one = nums[i];
                    int two = nums[j];
                    if (one % 2 == 0 && two % 2 == 0)
                    {
                        nums[i] = two;
                        nums[j] = one;
                    }
                    j--;
                }
            }
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            int result = 0;
            if (nums.Length >= 1)
            {
                int max = nums[0];
                int firstIndex = 0;
                int lastIndex = 0;

                for (int i = 1; i < nums.Length; i++)
                {
                    if (nums[i] > max)
                    {
                        max = nums[i];
                        firstIndex = i;
                    }
                    if (nums[i] == max)
                    {
                        lastIndex = i;
                    }
                }

                if (lastIndex != 0)
                {
                    result = lastIndex - firstIndex;
                }
            }
            if (nums.Length == 0 || nums.Length == 1)
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i>j)
                    {
                        matrix[i, j] = 0;
                    }
                    if (j>i)
                    {
                        matrix[i, j] = 1;
                    }
                }

            }
        }
    }
}
