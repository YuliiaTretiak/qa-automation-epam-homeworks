package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AppleStorePage extends BasePage {

    @FindBy(xpath = "//a[@href='https://avic.ua/ipad/seriya--ipad-mini-2021']")
    private WebElement iPadMiniButton;

    public AppleStorePage(WebDriver driver) {
        super(driver);
    }

    public void clickIPadMiniButton () {iPadMiniButton.click();}
}
