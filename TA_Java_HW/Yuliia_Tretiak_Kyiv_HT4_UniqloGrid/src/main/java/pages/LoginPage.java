package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{

    @FindBy(xpath = "//div[contains(@class,'emerald')]//div[@class=contains(text(),'Create an account')]")
    private WebElement createAccountForm;

    @FindBy(xpath = "//div[contains(@class,'emerald')]//div[@class=contains(text(),'Login')]")
    private WebElement loginButtonForm;

    @FindBy(xpath = "//form[@id='dwfrm_login']//div[@class='emerald__inputWrapper  ']")
    private WebElement emailAddressLine;

    @FindBy(xpath = "//div[contains(@class,'emerald__formBox')]//form//div[contains(@class,'inputWrapper--password')]")
    private WebElement enterPassword;

    @FindBy(xpath = "//button[contains(@class,'accountLoginButton')]")
    private WebElement loginButton;

    @FindBy(xpath = "//a[contains(@class,'Forget')]")
    private WebElement forgotThePassword;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCreateAccountFormVisible() {return createAccountForm.isDisplayed();}

    public boolean isLoginButtonFormVisible() {return loginButtonForm.isDisplayed();}

    public boolean isEmailAddressLineVisible(){return emailAddressLine.isDisplayed();}

    public boolean isEnterPasswordVisible(){return enterPassword.isDisplayed();}

    public boolean isLoginButtonVisible(){return loginButton.isDisplayed();}

    public boolean isForgotThePasswordVisible(){return forgotThePassword.isDisplayed();}

    public WebElement getLoginButtonForm(){return loginButtonForm;}
}
