package PageFactoryTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class iPadMiniTest extends BaseTest {

    private String KEYWORD = "iPad Mini";
    private String EXPECTED_PRODUCT_NAME = "MK7P3";
    private String EXPECTED_PRICE = "18399 грн";

    @Test
    public void checkThatChosenCategoryContainsRightProducts() {
        getHomePage().clickAppleStoreButton();
        getAppleStorePage().implicitWait(30);
        getAppleStorePage().clickIPadMiniButton();
        getIpadMiniPage().implicitWait(15);
        for (WebElement webElement : getIpadMiniPage().getIPadMiniList()) {
            assertTrue(webElement.getText().contains(KEYWORD));
        }

    }

    @Test (dependsOnMethods = "checkThatChosenCategoryContainsRightProducts")
    public void addToCart (){
    getIpadMiniPage().clickFilter64Gb();
    getIpadMiniPage().waitVisibilityOfElement(30, getIpadMiniPage().getFilterContains64GB());
    getIpadMiniPage().clickFilterContainsProduct();
    getIpadMini64GbPage().implicitWait(30);
    getIpadMini64GbPage().clickAddToCartButton();
    getIpadMini64GbPage().waitVisibilityOfElement(30, getIpadMini64GbPage().getAddToCartPopup());
    assertTrue(getIpadMini64GbPage().getProductName().contains(EXPECTED_PRODUCT_NAME));
    }

    @Test (dependsOnMethods = "addToCart")
    public void checkThePrice () {
        assertEquals(getIpadMini64GbPage().getCartPrice(), EXPECTED_PRICE);
    }
}
