package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UltraLightVestPage extends BasePage {

    @FindBy(xpath = "//h1[@class='pdp__title js_pdpTitle']")
    private WebElement lightVestProductName;

    @FindBy(xpath = "//button[@id='COL09']")
    private WebElement blackColorButton;

    @FindBy(xpath = "//button[contains(@class,'js_pdp-sizeChip')and@class=contains(text(),'M')]")
    private WebElement sizeMButton;

    @FindBy(xpath = "//form[contains(@id,'addtocart')]")
    private WebElement addToBagButton;

    @FindBy(xpath = "//div[contains(@class,'js_dialogWrapper')]")
    private WebElement addToBagPopUp;

    @FindBy(xpath = "//div[@class='addToCart__buttons']//a[contains(@title, 'checkout')]")
    private WebElement addToBagPopUpCheckoutButton;

    @FindBy(xpath = "//dd[@class='productItem__value']")
    private WebElement productQuantity;

    @FindBy(xpath = "//div[@id='onetrust-banner-sdk']")
    private WebElement cookiesPopUp;

    @FindBy(xpath = "//button[@id='onetrust-accept-btn-handler']")
    private WebElement acceptCookies;

    public UltraLightVestPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getLightVestProductName () {return lightVestProductName;}

    public String getLightVestProductNameText (){return lightVestProductName.getText();}

    public boolean isBlackColorButtonVisible() {return blackColorButton.isDisplayed();}

    public boolean isSizeMButtonVisible() {return sizeMButton.isDisplayed();}

    public boolean isAddToBagButtonVisible() {return addToBagButton.isDisplayed();}

}
