package tests;

import pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import utils.CapabilityFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {

    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
    private CapabilityFactory capabilityFactory = new CapabilityFactory();

    private static final String UNIQLO_URL = "https://www.uniqlo.com/uk/en/home";

    @BeforeMethod
    @Parameters(value = {"browser"})
    public void setUp(@Optional("firefox") String browser) throws MalformedURLException {
        driver.set(new RemoteWebDriver(new URL("http://192.168.31.38:4444/wd/hub"), capabilityFactory.getCapabilities(browser)));
        getDriver().manage().window().maximize();
        getDriver().get(UNIQLO_URL);
    }


    @AfterMethod
    public void tearDown() {
        getDriver().quit();
    }

    @AfterClass
    void terminate() {
        driver.remove();
    }

    public WebDriver getDriver() {
        return driver.get();
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public WomenPage getWomenPage () {return new WomenPage(getDriver());}

    public CoatsJacketsPage getCoatsJacketsPage () {return new CoatsJacketsPage(getDriver());}

    public UltraLightVestPage getUltraLightVestPage () {return new UltraLightVestPage(getDriver());}

    public LoginPage getLoginPage () {return new LoginPage(getDriver());}

    public SearchPage getSearchPage(){return new SearchPage(getDriver());}

}
