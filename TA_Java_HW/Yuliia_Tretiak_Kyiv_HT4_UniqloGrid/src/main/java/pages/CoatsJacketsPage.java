package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoatsJacketsPage extends BasePage {

    @FindBy(xpath = "//span[@class='productTile__heading']//a[@title='Women Ultra Light Down Vest']")
    private WebElement lightVest;

    public CoatsJacketsPage(WebDriver driver) {
        super(driver);
    }

    public void clickLightVest () {lightVest.click();}


}
