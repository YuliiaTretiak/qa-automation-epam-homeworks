package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Objects;

public class HomePage extends BasePage{

    @FindBy(xpath = "//header")
    private WebElement header;

    @FindBy(xpath = "//div//footer")
    private WebElement footer;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchField;

    @FindBy(xpath = "//a[@data-testid='women-floor']")
    private WebElement womenButton;

    @FindBy(xpath = "//a[@data-testid='men-floor']")
    private WebElement menButton;

    @FindBy(xpath = "//div[@id='myAccountDropdown']")
    private WebElement accountIcon;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement savedItemsIcon;

    @FindBy(xpath = "//a[@data-testid='miniBagIcon']")
    private WebElement cartIcon;

    @FindBy(xpath = "//li//button[@data-testid='country-selector-btn']")
    private WebElement countrySelectorButton;

    @FindBy(xpath = "//section[@data-testid='country-selector-form']")
    private WebElement countrySelectorForm;

    @FindBy(xpath = "//select[@id='country']")
    private WebElement countrySelectorDropdownMenu;

    @FindBy(xpath = "//select[@id='country']/option")
    private List<WebElement> countryItems;

    @FindBy(xpath = "//select[@id='currency']")
    private WebElement currencyDropdownMenu;

    @FindBy(xpath = "//select[@id='currency']/option")
    private List<WebElement> currencyItems;

    @FindBy(xpath = "//button[@data-testid='save-country-button']")
    private WebElement updatePreferenceButton;

    @FindBy(xpath = "//li//button[@data-testid='country-selector-btn']//img")
    private WebElement countryFlag;

    @FindBy(xpath = "//button[@data-testid='search-button-inline']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@data-testid='myaccount-link']")
    private WebElement myAccountButton;

    public HomePage(WebDriver driver) {super(driver);}

    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean isHeaderVisible() {
        return header.isDisplayed();
    }

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    public boolean isAccountIconVisible() {
        return accountIcon.isDisplayed();
    }

    public boolean isWomenButtonVisible() {
        return womenButton.isDisplayed();
    }

    public boolean isMenButtonVisible() {
        return menButton.isDisplayed();
    }

    public boolean isSavedItemsIconVisible() { return savedItemsIcon.isDisplayed();}

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }

    public boolean isCountrySelectorButtonVisible () { return countrySelectorButton.isDisplayed();}

    public void clickCountrySelector()
    {
        countrySelectorButton.click();
    }

    public boolean isCountrySelectorFormVisible() {
        return countrySelectorForm.isDisplayed();
    }

    public boolean isCountrySelectorDropdownMenuVisible() {
        return countrySelectorDropdownMenu.isDisplayed();
    }

    public void clickCountrySelectorDropdownMenu() {
        countrySelectorDropdownMenu.click();
    }

    public boolean isCountryVisible(final String country)
    {
        for (WebElement item: countryItems)
        {
            if (Objects.equals(item.getAttribute("value"), country)) { return true; }
        }
        return false;
    }

    public void clickCountry(final String country)
    {
        for (WebElement item: countryItems)
        {
            if (Objects.equals(item.getAttribute("value"), country)) { item.click(); }
        }
    }

    public boolean isCurrencySelectorDropdownMenuVisible() {
        return currencyDropdownMenu.isDisplayed();
    }

    public void clickCurrencySelectorDropdownMenu() {
        currencyDropdownMenu.click();
    }

    public void clickCurrency(final String currency)
    {
        for (WebElement item: currencyItems)
        {
            if (item.getText().contains(currency))
            { item.click(); }
        }
    }

    public boolean isUpdatePreferenceButtonVisible() {
        return updatePreferenceButton.isDisplayed();
    }

    public void clickUpdatePreferenceButton() {
        updatePreferenceButton.click();
    }


    public boolean checkCountryFlag (final String flag){
        if (!Objects.equals(countryFlag.getAttribute("alt"), flag)) { return false; }
        return true;
    }

    public  WebElement getCountrySelectorForm (){
        return countrySelectorForm;
    }

    public void clickWomenButton () { womenButton.click();}

    public void clickAccountIcon () { accountIcon.click();}

    public void clickMyAccountButton() { myAccountButton.click();}

    public WebElement getMyAccountButton() { return myAccountButton;}
}
