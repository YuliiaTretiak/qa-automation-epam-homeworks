package PageFactoryTests;

import Pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {

    private WebDriver driver;
    private static final String AVIC_URL = "https://avic.ua/";

    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }

    @BeforeTest
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

   // @AfterMethod
    //public void tearDown() {
     //   driver.close();
    //}

    public WebDriver getDriver() {
        return driver;
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public AppleStorePage getAppleStorePage () {return new AppleStorePage(getDriver());}

    public iPadMiniPage getIpadMiniPage (){return new iPadMiniPage(getDriver());}

    public iPadMini64GBPage getIpadMini64GbPage () {return new iPadMini64GBPage(getDriver());}


}
